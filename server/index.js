const { request } = require('express');
const express = require('express');
const bodyParser = require("body-parser");
const cors = require("cors");
const app = express();
const mysql = require('mysql');

const db = mysql.createPool({
    host:'localhost',
    user:'root',
    port:'3306',
    password:'root',
    database: 'sqldemodb'
});
app.use(cors());
app.use(express.json());
app.use(bodyParser.urlencoded({extended: true}));

app.get('/api/get', (req,res)=> {
    const sqlSelect = "SELECT * FROM codegenrate"
    db.query(sqlSelect, (err,result)=>{
    res.send(result);
});
})
app.get('/api/getclient',(req,res)=> {
    const sqlselectClient = "SELECT * FROM client"
    db.query(sqlselectClient,(err,result)=>{
        res.send(result);
    });
})
app.get('/api/getdrug',(req,res)=> {
    const sqlselectDrug = "SELECT * FROM drug"
    db.query(sqlselectDrug,(err,result)=>{
        res.send(result);
    });
})

app.post("/api/insert", (req, res) =>{
    const name = req.body.name;
    const address = req.body.address;
    const sqlInsert = "INSERT INTO codegenrate (drug_code, client_code) VALUES (?,?)";
    db.query(sqlInsert,[name,address],(err,result)=>{
        console.log(err);
    });
});
//app.get('/', (req, res)=> {
    /* const sqlInsert = "INSERT INTO category (name,address) VALUES ('Jyothi','Toronto');"
    /* db.query(sqlInsert, (err, result)=> { */
    /*      */
    /* }); */
    // res.send("Welcome")
//});

app.listen(3001, ()=> {
console.log("it is running");
});